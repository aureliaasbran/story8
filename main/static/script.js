$("#input").keyup( function() {
    var q = $("#input").val();
    var url_utk_dipanggil = '/data?q=' + q;
    $.ajax({
        url:url_utk_dipanggil,
        success: function(hasil){
            console.log(hasil.items);
            var hasil_input = $("#hasil");
            hasil_input.empty();

            for (i = 0; i < hasil.items.length; i++){
                var thumbnail = hasil.items[i].volumeInfo.imageLinks.smallThumbnail;
                var title = hasil.items[i].volumeInfo.title;
                var author = hasil.items[i].volumeInfo.authors;
                var publisher = hasil.items[i].volumeInfo.publisher;
                var publishedDate = hasil.items[i].volumeInfo.publishedDate;
                hasil_input.append("<tr> <th scope='row' class='align-middle text-center'>" + (i + 1) + "</th>" +
                "<td><img class='img-fluid' style='width:22vh' src='" + thumbnail+ "'></img>" + "</td>" +
                "<td class='align-middle'>" + title + "</td>" +
                "<td class='align-middle'>" + author + "</td>" +
                "<td class='align-middle'>" + publisher + "</td>" +
                "<td class='align-middle'>" + publishedDate + "</td>")
            }
        }
    })
})
//https://www.googleapis.com/books/v1/volumes?q=